package vendingmachine.step3;

import vendingmachine.step3.TextVendingMachineInterface;
import vendingmachine.step3.VendingMachineInterface;

//helpcodecommunity

import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        VendingMachineInterface machineInterface = new TextVendingMachineInterface();


        try {
            machineInterface.displayProducts();
            String selectedProduct = scanner.nextLine();
            machineInterface.selectProduct(Integer.parseInt(selectedProduct));

            machineInterface.displayEnterCoinsMessage();
            String userEnteredCoins = scanner.nextLine();

            try {
                int[] enteredCoins = Coin.parseCoins(userEnteredCoins);

                if (userEnteredCoins.length()>5) {
                    System.out.println("Input anda tidak sesuai ketentuan");
                } else {
                    machineInterface.enterCoins(enteredCoins);
                    machineInterface.displayChangeMessage();
                }
            } catch (NumberFormatException e) {
                System.out.println("Format yang anda masukkan mengandung karakter");
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Jumlah coins yang anda masukkan tidak valid");
            }

        } catch (NumberFormatException e) {
            System.out.println("Format yang anda masukkan bukan angka!");
        }
    }
}
