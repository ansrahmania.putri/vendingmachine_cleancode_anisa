package vendingmachine.step3;

public class CoinBundle {
    public int number5CentsCoins;
    public int number10CentsCoins;
    public int number20CentsCoins;
    public int number50CentsCoins;
    public int number100CentsCoins;
    private int total = 0;

    public CoinBundle(int... enteredCoins) {
        this.number5CentsCoins = enteredCoins[0];
        this.number10CentsCoins = enteredCoins[1];
        this.number20CentsCoins = enteredCoins[2];
        this.number50CentsCoins = enteredCoins[3];
        this.number100CentsCoins = enteredCoins[4];
    }
    public int getTotal(){
        total = total5CentsCoins();
        total = total10CentsCoins();
        total = total20CentsCoins();
        total = total50CentsCoins();
        total = total100CentsCoins();
        return total;
    }

    public int total5CentsCoins() {
        return total + this.number5CentsCoins * Coin.FIVE_CENTS.getValue();
    }

    public int total10CentsCoins() {
        return total + this.number10CentsCoins * Coin.TEN_CENTS.getValue();
    }

    public int total20CentsCoins() {
        return  total + this.number20CentsCoins * Coin.TWENTY_CENTS.getValue();
    }

    public int total50CentsCoins() {
        return total + this.number50CentsCoins * Coin.FIFTY_CENTS.getValue();
    }

    public int total100CentsCoins() {
        return total + this.number100CentsCoins * Coin.HUNDRED_CENTS.getValue();
    }
}
