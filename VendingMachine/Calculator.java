package vendingmachine.step3;

public interface Calculator {
    int calculateTotal(CoinBundle enteredCoins);
    int changeMoney(int returnMoney, int coinsValue);
    int moduloMoney(int returnMoney, int coinsValue);

    CoinBundle calculateChange(int enteredByUserMoney);
}
