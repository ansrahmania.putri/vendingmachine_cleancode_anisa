package vendingmachine.step3;

public class SimpleCalculator implements Calculator {
    private CoinBundle change;

    @Override
    public int calculateTotal(CoinBundle enteredCoins) {
        return enteredCoins.getTotal();
    }
    @Override
    public CoinBundle calculateChange(int amountMoneyToReturn) {
        change = new CoinBundle(new int[5]);
        int remainingAmount = amountMoneyToReturn;

        change.number100CentsCoins = changeMoney(remainingAmount, Coin.HUNDRED_CENTS.getValue());
        remainingAmount = moduloMoney(remainingAmount,Coin.HUNDRED_CENTS.getValue() );

        change.number50CentsCoins = changeMoney(remainingAmount, Coin.FIFTY_CENTS.getValue()) ;
        remainingAmount = moduloMoney(remainingAmount, Coin.FIFTY_CENTS.getValue());

        change.number20CentsCoins = changeMoney(remainingAmount, Coin.TWENTY_CENTS.getValue());
        remainingAmount = moduloMoney(remainingAmount, Coin.TWENTY_CENTS.getValue());

        change.number10CentsCoins = changeMoney(remainingAmount, Coin.TEN_CENTS.getValue());
        remainingAmount = moduloMoney(remainingAmount, Coin.TEN_CENTS.getValue());

        change.number5CentsCoins = changeMoney(remainingAmount, Coin.FIVE_CENTS.getValue());
        return change;
    }

    public int changeMoney(int returnMoney, int coinsValue) {
        return returnMoney / coinsValue;
    }

    public int moduloMoney(int returnMoney, int coinsValue) {
        return returnMoney % coinsValue;
    }

}
